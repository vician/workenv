#!/bin/bash

# Do not forget to specify variable gitserver in each workenv file!

gitclone () {
	gitdir=$1
	dir=$2
	if [ $# -eq 3 ]; then
		currentgitserver=$3
	else
		currentgitserver=$gitserver
	fi

	if [ ! -d "$dir" ]; then
		git clone $currentgitserver:$gitdir $dir
	else
		echo "$dir already exists"
	fi
	gitsubmodule $dir
}
export -f gitclone

gitsubmodule () {
	dir=$1
	cd $dir
	git submodule init
	git submodule update
	cd $HOME
}
export -f gitsubmodule

if [ $# -eq 0 ]; then
	echo "ERROR: Please specify workenv files as parameters"
	exit 1
fi

for param in $@; do
	if [ ! -f "$param" ]; then
		echo "ERROR: workenv file $param not found!"
		exit 1
	fi
	bash $param
done
